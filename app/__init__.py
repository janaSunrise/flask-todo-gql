from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .config import SQLALCHEMY_DB_URI

# Create the app
app = Flask(__name__)

# Config
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DB_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Database
db = SQLAlchemy(app)

# Close the db
@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()
