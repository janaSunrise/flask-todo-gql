import pkgutil
import types

from .. import models, views


def get_module_name(name: str) -> str:
    return name.split(".")[-1]


def get_modules_list(package: types.ModuleType):
    modules = []

    for submodule in pkgutil.walk_packages(package.__path__, f"{package.__name__}."):
        if get_module_name(submodule.name).startswith("_"):
            continue

        modules.append(submodule.name)

    return modules


# Autoload modules in the `models/` and `views/` folders
DATABASES = get_modules_list(models)
VIEWS = get_modules_list(views)
