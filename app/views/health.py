from flask import Blueprint, jsonify

# Blueprint
api = Blueprint("health", __name__, template_folder="templates", url_prefix="/health")

# Routes
@api.route("/")
def health_endpoint():
    return jsonify({"status": "OK"}), 200
