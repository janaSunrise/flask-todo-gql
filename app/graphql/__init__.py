import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyConnectionField

from .todo import TodoGQL, CreateTodo, DeleteTodo, UpdateTodo

# Query model
class Query(graphene.ObjectType):
    # Node
    node = relay.Node.Field()

    # Todo
    todo = SQLAlchemyConnectionField(TodoGQL.connection)


# Mutations
class Mutations(graphene.ObjectType):
    # Todo section
    create_todo = CreateTodo.Field()
    delete_todo = DeleteTodo.Field()
    update_todo = UpdateTodo.Field()


# Schema
schema = graphene.Schema(query=Query, mutation=Mutations)
