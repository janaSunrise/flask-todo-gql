import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from .. import db
from ..models.todo import Todo

# Todo interface
class TodoGQL(SQLAlchemyObjectType):
    class Meta:
        model = Todo
        interfaces = (relay.Node,)


# Mutations for performing operations using GraphQL
class CreateTodo(relay.ClientIDMutation):
    # Inputs from the mutation
    class Input:
        title = graphene.String(required=True)
        description = graphene.String(required=True)
        active = graphene.Boolean(required=False)
        created = graphene.DateTime(required=False)

    todo = graphene.Field(TodoGQL)

    # Method that performs the mutation with instructions based on input
    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        todo = Todo(
            title=input.get("title"),
            description=input.get("description"),
            active=input.get("active"),
            created=input.get("created"),
        )
        db.session.add(todo)
        db.session.commit()
        return CreateTodo(todo=todo)


class UpdateTodo(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)
        title = graphene.String(required=False)
        description = graphene.String(required=False)
        active = graphene.Boolean(required=False)

    todo = graphene.Field(TodoGQL)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        todo = Todo.query.get(input.get("id"))

        if todo:
            todo.title = input.get("title")
            todo.description = input.get("description")
            todo.active = input.get("active")

            db.session.add(todo)
            db.session.commit()

            return UpdateTodo(todo=todo)

        return UpdateTodo(todo=None)


class DeleteTodo(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)

    todo = graphene.Field(TodoGQL)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        todo = Todo.query.get(input.get("id"))

        if todo:
            db.session.delete(todo)
            return DeleteTodo(todo=todo)

        return DeleteTodo(todo=None)
