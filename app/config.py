HOST = "127.0.0.1"
PORT = 5000
DEBUG = True

# Database
SQLALCHEMY_DB_URI = f"sqlite:///app.db"
