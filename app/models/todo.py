from datetime import datetime

from app import db

# Database table model
class Todo(db.Model):
    # Define the name of the table
    __tablename__ = "todo"

    id = db.Column(db.Integer, primary_key=True, unique=True)

    # Info
    title = db.Column(db.String(80))
    description = db.Column(db.String(255))

    # Active
    active = db.Column(db.Boolean, default=True)

    # Created
    created = db.Column(db.DateTime, default=datetime.now)

    # Methods
    def __repr__(self):
        return f"<Todo id={self.id} title={self.title}>"
