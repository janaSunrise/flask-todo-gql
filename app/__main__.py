from importlib import import_module

from flask_graphql import GraphQLView

from . import app, db
from .config import HOST, PORT, DEBUG
from .core.loader import VIEWS
from .graphql import schema
from .models import bring_databases_into_scope

# GraphQL attach
app.add_url_rule(
    "/graphql", view_func=GraphQLView.as_view("graphql", schema=schema, graphiql=True)
)

# Import databases
bring_databases_into_scope()  # Load the databases into scope for SQLAlchemy to auto-detect and use

# Create all tables
db.create_all()

# Views
for view in VIEWS:
    # Import the view
    blueprint = import_module(view)

    # Register if has the `api` attribute.
    if hasattr(blueprint, "api"):
        app.register_blueprint(blueprint.api)
    else:
        print(f"[WARNING] View {blueprint.__name__} included but not found")

# Run the app
app.run(host=HOST, port=PORT, debug=DEBUG)
