# Flask TODO GraphQL

A simple API for TODOs to perform CRUD operations using GraphQL.

## Installation and Usage

This project uses `pipenv` for dependency management.

Here's how to install the dependencies and get started,

- Install dependencies using `pipenv install`.
- Run the flask app using `pipenv run start`.

And, you should be good to go.
